import * as ec2 from '@aws-cdk/aws-ec2';
import * as elbv2 from '@aws-cdk/aws-elasticloadbalancingv2';
import * as iam from '@aws-cdk/aws-iam';
import * as s3 from '@aws-cdk/aws-s3';
import * as targets from '@aws-cdk/aws-elasticloadbalancingv2-targets'
import * as cdk from '@aws-cdk/core';

export class HelloWorldStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create new S3 bucket (because why not)
    new s3.Bucket(this, 'MyFirstBucket', {
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED
    })

    // Create new VPC to launch our EC2 instance in
    const vpc = new ec2.Vpc(this, 'VPC', {
      subnetConfiguration: [
        {
          name: 'Public',
          subnetType: ec2.SubnetType.PUBLIC
        },
        {
          name: 'Private',
          subnetType: ec2.SubnetType.PRIVATE
        },
        {
          name: 'Isolated',
          subnetType: ec2.SubnetType.ISOLATED
        }
      ]
    });

    // Find the latest Amazon Linux 2 AMI
    const amznLinux = ec2.MachineImage.latestAmazonLinux();

    // Define user data for EC2 instance
    const userData = ec2.UserData.forLinux()
    userData.addCommands(
      'yum update -y',
      'yum install -y httpd',
      'chkconfig httpd on',
      'service httpd start',
      'echo "Hello World!" > /var/www/html/index.html',
      )

    // Launch an EC2 instance using the AMI above in the VPC defined above
    const mainInstance = new ec2.Instance(this, 'MyFirstEc2Instance', {
      vpc,
      vpcSubnets: { subnetType: ec2.SubnetType.PRIVATE },
      instanceType: new ec2.InstanceType('t3.medium'),
      machineImage: amznLinux,
      userData: userData
    })

    // Add SSM permissions to role
    mainInstance.role.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'));

    // Create NLB
    const nlb = new elbv2.NetworkLoadBalancer(this, 'MyFirstNetworkLoadBalancer', {
      vpc,
      internetFacing: true,
    })

    // Add listener on port 443 for NLB
    const nlbListener = nlb.addListener('MyFirstNLBListener',
      {
        port: 80,
        protocol: elbv2.Protocol.TCP
      })

    // Create target group and add instance
    const nlbTargetGroup = nlbListener.addTargets('MyFirstNLBTargetGroup', {
      port: 80,
      targets: [new targets.InstanceTarget(mainInstance)],
    })

    // // Change target group protocol to TLS
    // const CfnNlbTargetGroup = nlbTargetGroup.node.defaultChild as elbv2.CfnTargetGroup
    // CfnNlbTargetGroup.addPropertyOverride('Protocol', 'TLS')

    // Add rule allowing the world to communicate with our instances
    mainInstance.connections.allowFromAnyIpv4(ec2.Port.tcp(80))
  }
}
